FROM node:12.18.3-alpine as build

WORKDIR /app

COPY package.json ./
RUN npm i --silent --production
COPY . ./
RUN npm run build

# production environment
FROM nginx:alpine

RUN apk update && apk add bash

COPY --from=build /app/build /usr/share/nginx/html
# new
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]